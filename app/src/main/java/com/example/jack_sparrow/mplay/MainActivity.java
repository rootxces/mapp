package com.example.jack_sparrow.mplay;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.constraint.solver.widgets.WidgetContainer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jack_sparrow.mplay.dummy.DummyContent;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SongListFragment.OnListFragmentInteractionListener {
    int layoutGenerator = 0;
    LinearLayout songList;
    LinearLayout songPlayer;

    final static int REQUEST_EXTERNAL_STORAGE = 1;
    final static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    MediaPlayer mediaPlayer;
    String filePath;
    int songIndex=0;
    final String[] songPath = new String[200];
    public int numOfSongs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        verifyStoragePermissions(this);
        int songsStatus = findSongs();
        if(songsStatus == 1){
            for(int i = 0; i<numOfSongs ; i++){
                songPath[i] = DummyContent.songs[i];
                //Log.i("TAG", "\nSong: "+songPath[i]);
            }
            filePath = songPath[0];
            createMediaPlayer();

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.release();
                }
            });

            Button playButton = (Button) findViewById(R.id.play_button);
            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaPlayer.start();
                }
            });

            Button pauseButton = (Button) findViewById(R.id.pause_button);
            pauseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaPlayer.pause();
                }
            });

            Button seekForward = (Button) findViewById(R.id.forward_button);
            seekForward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);
                }
            });

            Button seekReverse = (Button) findViewById(R.id.reverse_button);
            seekReverse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 5000);
                }
            });

            Button stop = (Button) findViewById(R.id.stop_button);
            stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaPlayer.seekTo(0);
                    mediaPlayer.pause();
                }
            });
            Button next = (Button) findViewById(R.id.next_button);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaPlayer.release();
                    songIndex++;
                    if(songIndex<numOfSongs){
                        filePath = songPath[songIndex];
                        createMediaPlayer();
                        mediaPlayer.start();
                    }
                    else{
                        songIndex = 0;
                        filePath = songPath[songIndex];
                        createMediaPlayer();
                        mediaPlayer.start();
                    }


                }
            });

            Button reverse = (Button) findViewById(R.id.prev_button);
            reverse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mediaPlayer.release();
                    songIndex--;
                    if(songIndex>-1){
                        filePath = songPath[songIndex];
                        createMediaPlayer();
                        mediaPlayer.start();
                    }
                    else{
                        songIndex = numOfSongs-1;
                        filePath = songPath[songIndex];
                        createMediaPlayer();
                        mediaPlayer.start();
                    }
                }
            });
        }
        else{
            //Log.i("TAG","This is just a test message");
            Toast.makeText(this, "Song List is empty!", Toast.LENGTH_LONG).show();

        }
    }

    public void createMediaPlayer(){
        this.mediaPlayer = MediaPlayer.create(this, Uri.parse(Environment.getExternalStorageDirectory()
                .getPath()+"/Music/"+filePath));
        this.mediaPlayer.setLooping(true);
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    public void switchActivity(View view) {
        ++layoutGenerator;
        if (layoutGenerator == 1) {
            songList = (LinearLayout) findViewById(R.id.viewC);
            songPlayer = (LinearLayout) findViewById(R.id.viewB);
            songPlayer.setVisibility(view.VISIBLE);
            songList.setVisibility(View.GONE);
            android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.viewB, new SongListFragment());
            ft.commit();
        }
        else{
            songList = (LinearLayout) findViewById(R.id.viewC);
            songPlayer = (LinearLayout) findViewById(R.id.viewB);
            songPlayer.setVisibility(view.GONE);
            songList.setVisibility(View.VISIBLE);
            layoutGenerator=0;
        }
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        mediaPlayer.release();
        songIndex =Integer.parseInt(item.details);
        filePath = songPath[songIndex];
        createMediaPlayer();
        mediaPlayer.start();
        switchActivity(getCurrentFocus());
    }

    @Override
    public void onBackPressed() {
//        Toast.makeText(this, "Bye Bye!", Toast.LENGTH_SHORT).show();
        finish();

    }

    public int findSongs(){
        String path = Environment.getExternalStorageDirectory().getPath().toString()+"/Music";
        //Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        numOfSongs = files.length;
        Log.d("Files", "Size: "+ files.length);
        if(numOfSongs!=0){
            for (int i = 0; i < files.length; i++)
            {
                Log.d("Files", "FileName:" + files[i].getName());
                if(files[i].toString().contains(".mp3")){
                    DummyContent.songs[i] = files[i].getName();
                    DummyContent.addItem(DummyContent.createDummyItem(i));
                }
            }
            return 1;
        }
        else{
            return 0;
        }

    }
}
