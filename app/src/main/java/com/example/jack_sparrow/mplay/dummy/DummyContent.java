package com.example.jack_sparrow.mplay.dummy;

import android.os.Environment;
import android.util.Log;

import com.example.jack_sparrow.mplay.MainActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

   // private static final int COUNT = 200 ;
    public  static String[] songs = new String[200];


    /*static {
        songs[0] ="1.One_dance.mp3";
        songs[1] = "2.Let_me_love_you.mp3";
        songs[2] = "3.Honda jo dimag dil da.mp3";
        songs[3] = "4.Rockabye baby.mp3";
        songs[4] = "5.Tu hi saccha ve.mp3";
        songs[5] = "6.Fifty shades of grey.mp3";
        songs[6] = "7.It ain't me.mp3";
        songs[7] = "8.Slide.mp3";
        songs[8] = "9.21392039.mp3";
        songs[9] = "10.Do you love me.mp3";
        songs[10] = "11.21426019.mp3";
        songs[11] = "12.Mana k hum yar nahi.mp3";
        songs[12] = "13.Fiyya.mp3";
        songs[13] = "14.21464115.mp3";
        songs[14] = "15.beat.mp3";
        // Add some sample items.
        for (int i = 0; i < COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }*/

    /*public static void findSongs(){
        String path = Environment.getExternalStorageDirectory().getPath().toString()+"/Music";
        //Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
       // Log.d("Files", "Size: "+ files.length);
        for (int i = 0; i < files.length; i++)
        {
            //Log.d("Files", "FileName:" + files[i].getName());
            songs[i] = files[i].getName();
            addItem(createDummyItem(i));
        }
    }*/

    public static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.songName, item);
    }

    public static DummyItem createDummyItem(int position) {
        return new DummyItem(String.valueOf(position), songs[position], makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("*****");
        return builder.toString();
    }


    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String songName;
        public final String rating;
        public final String details;

        public DummyItem(String details, String songName, String rating) {
            this.songName = songName;
            this.rating = rating;
            this.details = details;
        }

        @Override
        public String toString() {
            return songName;
        }
    }


}
